
import 'package:flutter/material.dart';
import '../features/shrine/shrine.dart';
import '../features/english_words/english_words.dart';

import '../features/home_page.dart';

/// @author Created by Muhamad Jalaludin on 19/08/2022

class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return CustomRoute(builder: (_) => const MyHomePage(title: 'Flutter Codelabs'));
      case '/english_words':
        return CustomRoute(builder: (_) => const EnglishWordsApp());
      case '/english_words_home':
        return CustomRoute(builder: (_) => const EnglishWordsPage(title: 'English Words'));
      case '/shrine':
        return CustomRoute(builder: (_) => const ShrineApp());
      case '/login_shrine':
        return CustomRoute(builder: (_) => const LoginPage());
      // case '/shrine_home':
      //   return CustomRoute(builder: (_) => Backdrop(
      //     currentCategory: Category.all,
      //     frontLayer: HomePage(category: category),
      //     backLayer: CategoryMenuPage(
      //         currentCategory: Category.all,
      //         onCategoryTap: onCategoryTap
      //     ),
      //     frontTitle: const Text('SHRINE'),
      //     backTitle: const Text('MENU'),
      //   ));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_)  {
      return Scaffold(
        appBar: AppBar(title: const Text('Error')),
        body: const Center(child: Text('Error page')),
      );
    });
  }

  static Route _createRoute(Widget page) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        const curve = Curves.ease;
        
        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 1000)
    );
  }
}

class CustomRoute<T> extends MaterialPageRoute<T> {
  CustomRoute({required WidgetBuilder builder, RouteSettings? settings})
      : super(builder: builder, settings: settings);
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }
}