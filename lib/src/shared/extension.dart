import 'package:flutter/material.dart';
import 'package:flutter_codelabs/src/shared/colors.dart';
import '../shared/views/views.dart';

/// @author Created by Muhamad Jalaludin on 23/08/2022

extension ThemeUtils on BuildContext {
  ThemeData get base => ThemeData.light();
  ThemeData get theme => base.copyWith(
      colorScheme: base.colorScheme.copyWith(
          primary: kShrinePink100,
          onPrimary: kShrineBrown900,
          secondary: kShrineBrown900,
          error: kShrineErrorRed
      ),
      textTheme: textTheme,
      textSelectionTheme: const TextSelectionThemeData(
          selectionColor: kShrineBrown900
      ),
      inputDecorationTheme: inputDecorationTheme
  );
  InputDecorationTheme? get inputDecorationTheme => const InputDecorationTheme(
      border: CutCornersBorder(),
      focusedBorder: CutCornersBorder(
          borderSide: BorderSide(
              width: 2,
              color: kShrineBrown900
          )
      ),
      floatingLabelStyle: TextStyle(
          color: kShrineBrown900
      )
  );
  TextTheme get textTheme => base.textTheme.apply(
      fontFamily: 'Rubik',
      displayColor: kShrineBrown900,
      bodyColor: kShrineBrown900
  );
  TextStyle? get headline5 => textTheme.headline5?.copyWith(
      fontWeight: FontWeight.w500
  );
  TextStyle? get headline6 => textTheme.headline6?.copyWith(
      fontSize: 18
  );
  TextStyle? get caption => textTheme.caption?.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14
  );
  TextStyle? get bodyText1 => textTheme.bodyText1?.copyWith(
      fontWeight: FontWeight.w500,
      fontSize: 16
  );
}