import 'package:flutter/material.dart';

/// @author Created by Muhamad Jalaludin on 19/08/2022

class Widgets {
  static PreferredSizeWidget customAppBar(String title) {
    return AppBar(
      title: Text(title),
    );
  }

}

class CustomButton extends StatelessWidget {
  final String label;
  final VoidCallback? listener;

  const CustomButton(this.label, this.listener, {super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: listener,
      child: Text(label),
    );
  }
}