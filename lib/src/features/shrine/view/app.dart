import 'package:flutter/material.dart';
import 'package:flutter_codelabs/src/shared/extension.dart';

import '../../../shared/routes.dart';
import '../../../shared/classes/classes.dart';
import '../../english_words/english_words.dart';
import '../../shrine/shrine.dart';
import '../../home_page.dart';

/// @author Created by Muhamad Jalaludin on 23/08/2022

class ShrineApp extends StatefulWidget {
  const ShrineApp({Key? key}) : super(key: key);

  @override
  State<ShrineApp> createState() => _ShrineAppState();
}

class _ShrineAppState extends State<ShrineApp> {
  Category _currentCategory = Category.all;

  void _onCategoryTap(Category category) {
    setState(() {
      _currentCategory = category;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shrine',
      theme: ThemeUtils(context).theme,
      initialRoute: '/login_shrine',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/':
            return CustomRoute(builder: (_) => const MyHomePage(title: 'Flutter Codelabs'));
          case '/english_words':
            return CustomRoute(builder: (_) => const EnglishWordsApp());
          case '/english_words_home':
            return CustomRoute(builder: (_) => const EnglishWordsPage(title: 'English Words'));
          case '/shrine':
            return CustomRoute(builder: (_) => const ShrineApp());
          case '/login_shrine':
            return CustomRoute(builder: (_) => const LoginPage());
          case '/shrine_home':
            return CustomRoute(builder: (_) => Backdrop(
              currentCategory: _currentCategory,
              frontLayer: HomePage(category: _currentCategory),
              backLayer: CategoryMenuPage(
                  currentCategory: _currentCategory,
                  onCategoryTap: _onCategoryTap
              ),
              frontTitle: const Text('SHRINE'),
              backTitle: const Text('MENU'),
            ));
          default:
            // return _errorRoute();
            return null;
        }
      },
      debugShowCheckedModeBanner: false,
    );
  }
}

