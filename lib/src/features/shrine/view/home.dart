import 'package:flutter/material.dart';
import '../../../shared/providers/products.dart';
import '../../../shared/classes/classes.dart';
import '../../../shared/views/views.dart';

/// @author Created by Muhamad Jalaludin on 23/08/2022

class HomePage extends StatelessWidget {
  final Category? category;

  const HomePage({
    this.category = Category.all,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productProvider = ProductsProvider();
    final products = productProvider.loadProducts(category);

    return AsymmetricView(
      products: products,
    );
  }
}
