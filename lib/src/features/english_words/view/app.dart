import 'package:flutter/material.dart';

import '../../../shared/routes.dart';

/// @author Created by Muhamad Jalaludin on 23/08/2022

class EnglishWordsApp extends StatelessWidget {
  const EnglishWordsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'English Words',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
            backgroundColor: Colors.white,
            foregroundColor: Colors.black
        ),
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/english_words_home',
      onGenerateRoute: RouteGenerator.generateRoute,
      debugShowCheckedModeBanner: false,
    );
  }
}