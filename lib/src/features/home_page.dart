import 'package:flutter/material.dart';

import '../shared/widgets.dart';

/// @author Created by Muhamad Jalaludin on 19/08/2022

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Widgets.customAppBar(title),
      body: Center(
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            CustomButton("English Words", () {
              Navigator.of(context).pushNamed('/english_words');
            }),
            CustomButton("Shrine App", () {
              Navigator.of(context).pushNamed('/shrine');
            }),
          ],
        ),
      ),
    );
  }
}