import 'package:flutter/material.dart';
import 'package:flutter_codelabs/src/shared/routes.dart';

void main() {
  runApp(MaterialApp(
    title: 'Flutter Codelabs',
    theme: ThemeData(
      appBarTheme: const AppBarTheme(
        backgroundColor: Colors.blue,
        foregroundColor: Colors.black
      ),
      primarySwatch: Colors.blue,
    ),
    initialRoute: '/',
    onGenerateRoute: RouteGenerator.generateRoute,
    debugShowCheckedModeBanner: false,
  ));
}